--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: abstracts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE abstracts (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    body text
);


ALTER TABLE public.abstracts OWNER TO postgres;

--
-- Name: abstracts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE abstracts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.abstracts_id_seq OWNER TO postgres;

--
-- Name: abstracts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE abstracts_id_seq OWNED BY abstracts.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY abstracts ALTER COLUMN id SET DEFAULT nextval('abstracts_id_seq'::regclass);


--
-- Name: abstracts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY abstracts
    ADD CONSTRAINT abstracts_pkey PRIMARY KEY (id);


--
-- Name: abstracts_title_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY abstracts
    ADD CONSTRAINT abstracts_title_key UNIQUE (title);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: abstracts; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE abstracts FROM PUBLIC;
REVOKE ALL ON TABLE abstracts FROM postgres;
GRANT ALL ON TABLE abstracts TO postgres;
GRANT ALL ON TABLE abstracts TO abstracts;


--
-- Name: abstracts_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE abstracts_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE abstracts_id_seq FROM postgres;
GRANT ALL ON SEQUENCE abstracts_id_seq TO postgres;
GRANT SELECT,USAGE ON SEQUENCE abstracts_id_seq TO abstracts;


--
-- PostgreSQL database dump complete
--

