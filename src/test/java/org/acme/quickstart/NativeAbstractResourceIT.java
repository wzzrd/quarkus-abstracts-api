package org.acme.quickstart;

import io.quarkus.test.junit.SubstrateTest;

@SubstrateTest
public class NativeAbstractResourceIT extends AbstractResourceTest {

    // Execute the same tests but in native mode.
}
