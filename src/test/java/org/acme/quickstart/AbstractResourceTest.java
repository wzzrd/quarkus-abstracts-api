package org.acme.quickstart;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class AbstractResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/abstracts/get")
          .then()
             .statusCode(200);
    }

}
