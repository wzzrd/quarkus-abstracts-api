package org.acme.quickstart;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import io.reactiverse.axle.pgclient.*;

public class Abstract {

    public Long id;
    public String title;
    public String body;

    public Abstract() {
    }

    public Abstract(String title) {
        this.title = title;
    }

    public Abstract(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public Abstract(Long id, String title, String body) {
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public static CompletionStage<List<Abstract>> findAll(PgPool client) {
        return client.query("SELECT id, title, body FROM abstracts ORDER BY id ASC").thenApply(pgRowSet -> {
            List<Abstract> list = new ArrayList<>(pgRowSet.size());
            for (Row row : pgRowSet) {
                list.add(from(row));
            }
            return list;
        });
    }

    public CompletionStage<Long> save(PgPool client) {
        return client.preparedQuery("INSERT INTO abstracts (title, body) VALUES ($1, $2) RETURNING (id)", Tuple.of(title, body))
            .thenApply(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
    }

    public static CompletionStage<Abstract> findById(PgPool client, Long id) {
        return client.preparedQuery("SELECT id, title, body FROM abstracts WHERE id = $1", Tuple.of(id))
            .thenApply(PgRowSet::iterator)
            .thenApply(iterator -> iterator.hasNext() ? from(iterator.next()) : null);
    }

    private static Abstract from(Row row) {
        return new Abstract(row.getLong("id"), row.getString("title"), row.getString("body"));
    }
}
