package org.acme.quickstart;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AbstractService {

    public String greeting(int id) {
        Integer myInt = new Integer(id);
        String stringid = Integer.toString(myInt);
        return "hello " + stringid + "\n";
    }

}
