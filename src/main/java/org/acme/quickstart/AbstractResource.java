package org.acme.quickstart;

import io.vertx.axle.core.Vertx;
import javax.inject.Inject;
import java.net.URI;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import java.util.concurrent.CompletionStage;
import io.reactiverse.axle.pgclient.*;

@Path("/abstracts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AbstractResource {

    @Inject
    AbstractService service;

    @Inject Vertx vertx;
    
    @Inject
    io.reactiverse.axle.pgclient.PgPool client;

    @POST
    @Path("/save")
    public CompletionStage<Response> create(Abstract abstrakt) {
        System.out.printf("Calling post method%n");
        return abstrakt.save(client)
            .thenApply(id -> URI.create("/abstracts/" + id))
            .thenApply(uri -> Response.created(uri).build());
    }

    @DELETE
    @Path("/delete/{id}")
    public CompletionStage<Boolean> removeSingle(@PathParam("id") Long id) {
        System.out.printf("Calling delete method with id='%d' as parameter%n", id);
        return client.preparedQuery("DELETE FROM abstracts WHERE id = $1", Tuple.of(id))
            .thenApply(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    @GET
    @Path("/get/{id}")
    public CompletionStage<Response> getSingle(@PathParam("id") Long id) {
        System.out.printf("Calling get method with id='%d' as parameter%n", id);
        return Abstract.findById(client, id)
            .thenApply(abstrakt -> abstrakt != null ? Response.ok(abstrakt) : Response.status(Status.NOT_FOUND))
            .thenApply(ResponseBuilder::build);
    }

    @GET
    @Path("/get")
    public CompletionStage<Response> get() {
        System.out.printf("Calling get method without parameters%n");
        return Abstract.findAll(client)
            .thenApply(Response::ok)
            .thenApply(ResponseBuilder::build);
    }

}
