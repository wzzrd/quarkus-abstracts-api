function refresh() {
    $.get('/abstracts/get', function (abstracts) {
        var list = '';
        (abstracts || []).forEach(function (abstract) {
            list = list
            + '<tr>'
            + '<td>' + abstract.id + '</td>'
            + '<td>' + abstract.title + '</td>'
            + '<td>' + abstract.body + '</td>'
            + '<td><a href="#" onclick="deleteAbstract(' + abstract.id + ')">Delete</a></td>'
            + '</tr>'
        });
        if (list.length > 0) {
            list = ''
        + '<table><thead><th>Id</th><th>Title</th><th>Body</th><th></th></thead>'
        + list
        + '</table>';
        } else {
            list = "No abstracts in database"
        }
        $('#all-abstracts').html(list);
    });
}

function deleteAbstract(id) {
    $.ajax('/abstracts/delete/' + id, {method: 'DELETE'}).then(refresh);
}


$(document).ready(function () {

    $('#create-abstract-button').click(function () {
        var abstractTitle = $('#abstract-title').val();
        var abstractBody = $('#abstract-body').val();
        $.post({
            url: '/abstracts/save',
            contentType: 'application/json',
            data: JSON.stringify({title: abstractTitle, body: abstractBody})
        }).then(refresh);
    });

    refresh();
});

